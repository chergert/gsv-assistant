project('test-assistant', 'c', 
          version: '0.1.0',
    meson_version: '>= 0.50.0',
  default_options: [ 'warning_level=2',
                     'c_std=gnu11',
                   ],
)

gnome = import('gnome')
i18n = import('i18n')
cc = meson.get_compiler('c')

config_h = configuration_data()
config_h.set_quoted('PACKAGE_VERSION', meson.project_version())
config_h.set_quoted('GETTEXT_PACKAGE', 'test-assistant')
config_h.set_quoted('LOCALEDIR', join_paths(get_option('prefix'), get_option('localedir')))
configure_file(
  output: 'config.h',
  configuration: config_h,
)
add_project_arguments([
  '-I' + meson.build_root(),
], language: 'c')

core_enums_header = '''

#if defined (GTK_SOURCE_COMPILATION)
# include "config.h"
#endif

#if !defined (GTK_SOURCE_H_INSIDE) && !defined (GTK_SOURCE_COMPILATION)
# error "Only <gtksourceview/gtksource.h> can be included directly."
#endif

'''

core_enums = gnome.mkenums_simple('gtksource-enumtypes',
            sources: [ 'gtksourcecompletioncell.h', 'gtksourcecompletioncontext.h' ],
  identifier_prefix: 'GtkSource',
      symbol_prefix: 'gtk_source',
      header_prefix: core_enums_header,
          decorator: 'extern',
)

test_assistant_sources = [
  'gtksourcesignalgroup.c',
  'gtksourcebindinggroup.c',
  'gtksourceassistant.c',
  'gtksourceassistantchild.c',
  'gtksourcecompletion.c',
  'gtksourcecompletioncell.c',
  'gtksourcecompletionlistbox.c',
  'gtksourcecompletionlistboxrow.c',
  'gtksourcecompletioncontext.c',
  'gtksourcecompletionprovider.c',
  'gtksourcecompletionproposal.c',
  'gtksourcecompletioninfo.c',
  'gtksourcecompletionlist.c',
  'gtksourceview-assistants.c',
  'gtksourceview.c',
  'gtksourceutils.c',
  'test-provider.c',
  'main.c',
  core_enums,
]

test_assistant_deps = [
  dependency('gio-2.0', version: '>= 2.62'),
  dependency('gtk4', version: '>= 3.98'),
  cc.find_library('m', required: false),
]

gnome = import('gnome')

test_assistant_sources += gnome.compile_resources('test_assistant-resources',
  'test_assistant.gresource.xml',
  c_name: 'test_assistant'
)

executable('test-assistant', test_assistant_sources,
  c_args: [ '-DGTK_SOURCE_COMPILATION', '-Wno-unused-parameter', '-Wno-unused-variable' ],
  dependencies: test_assistant_deps,
  install: true,
)
