#pragma once

#include <gio/gio.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK_SOURCE_AVAILABLE_IN_ALL extern
#define GTK_SOURCE_AVAILABLE_IN_5_0 extern

typedef struct _GtkSourceBuffer             GtkSourceBuffer;
typedef struct _GtkSourceCompletion         GtkSourceCompletion;
typedef struct _GtkSourceCompletionCell     GtkSourceCompletionCell;
typedef struct _GtkSourceCompletionContext  GtkSourceCompletionContext;
typedef struct _GtkSourceCompletionInfo     GtkSourceCompletionInfo;
typedef struct _GtkSourceCompletionProposal GtkSourceCompletionProposal;
typedef struct _GtkSourceCompletionProvider GtkSourceCompletionProvider;
typedef struct _GtkSourceLanguage           GtkSourceLanguage;
typedef struct _GtkSourceView               GtkSourceView;

G_END_DECLS
