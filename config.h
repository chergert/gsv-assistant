#define GSV_API_VERSION "5"
#define GTK_SOURCE_BUFFER(o) ((GtkSourceBuffer *)o)
#define GTK_SOURCE_IS_BUFFER(o) (GTK_IS_TEXT_BUFFER(o))
#define G_LOG_DOMAIN "GtkSourceView"
