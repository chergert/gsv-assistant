#pragma once

#include "gtksourceview.h"
#include "gtksourceassistant-private.h"

G_BEGIN_DECLS

typedef struct
{
	GtkSourceView *view;
	GQueue         queue;
} GtkSourceViewAssistants;

void     _gtk_source_view_add_assistant            (GtkSourceView           *view,
                                                    GtkSourceAssistant      *assistant);
void     _gtk_source_view_remove_assistant         (GtkSourceView           *view,
                                                    GtkSourceAssistant      *assistant);
void     _gtk_source_view_assistants_init          (GtkSourceViewAssistants *assistants,
                                                    GtkSourceView           *view);
void     _gtk_source_view_assistants_add           (GtkSourceViewAssistants *assistants,
                                                    GtkSourceAssistant      *assistant);
void     _gtk_source_view_assistants_remove        (GtkSourceViewAssistants *assistants,
                                                    GtkSourceAssistant      *assistant);
void     _gtk_source_view_assistants_remove        (GtkSourceViewAssistants *assistants,
                                                    GtkSourceAssistant      *assistant);
void     _gtk_source_view_assistants_shutdown      (GtkSourceViewAssistants *assistants);
void     _gtk_source_view_assistants_size_allocate (GtkSourceViewAssistants *assistants,
                                                    int                      width,
                                                    int                      height,
                                                    int                      baseline);
gboolean _gtk_source_view_assistants_handle_key    (GtkSourceViewAssistants *assistant,
                                                    guint                    keyval,
                                                    GdkModifierType          state);

G_END_DECLS
