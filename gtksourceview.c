#include "gtksourceview.h"

#include "gtksourceassistant-private.h"
#include "gtksourcecompletion-private.h"
#include "gtksourceview-private.h"
#include "gtksourceutils-private.h"

typedef struct
{
	GtkSourceViewAssistants  assistants;
	GtkSourceCompletion     *completion;
	PangoFontDescription    *font_desc;
	GtkCssProvider          *css_provider;
} GtkSourceViewPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (GtkSourceView, gtk_source_view, GTK_TYPE_TEXT_VIEW)

GtkWidget *
gtk_source_view_new (void)
{
	return g_object_new (GTK_SOURCE_TYPE_VIEW, NULL);
}

GtkSourceCompletion *
get_completion (GtkSourceView *self)
{
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (self);

	if (priv->completion == NULL)
	{
		priv->completion = _gtk_source_completion_new (self);
		_gtk_source_completion_set_font_desc (priv->completion, priv->font_desc);
		g_object_set (priv->completion, "select-on-show", TRUE, NULL);
	}

	return priv->completion;
}

static void
show_completion_cb (GtkWidget   *widget,
                    const gchar *action_name,
                    GVariant    *state)
{
	GtkSourceView *view = GTK_SOURCE_VIEW (widget);
	GtkSourceCompletion *completion = get_completion (view);

	g_print ("Requesting completions\n");

	gtk_source_completion_show (completion);
}

static void
gtk_source_view_size_allocate (GtkWidget *widget,
                               int        width,
                               int        height,
                               int        baseline)
{
	GtkSourceView *view = GTK_SOURCE_VIEW (widget);
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (view);

	GTK_WIDGET_CLASS (gtk_source_view_parent_class)->size_allocate (widget, width, height, baseline);

	_gtk_source_view_assistants_size_allocate (&priv->assistants, width, height, baseline);
}

static gboolean
on_key_pressed_cb (GtkEventControllerKey *controller,
                   guint                  keyval,
                   guint                  keycode,
                   GdkModifierType        state,
                   GtkSourceView         *view)
{
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (view);

	g_assert (GTK_IS_EVENT_CONTROLLER_KEY (controller));
	g_assert (GTK_SOURCE_IS_VIEW (view));

	if (_gtk_source_view_assistants_handle_key (&priv->assistants, keyval, state))
	{
		return TRUE;
	}

	return FALSE;
}


static void
gtk_source_view_dispose (GObject *object)
{
	GtkSourceView *view = GTK_SOURCE_VIEW (object);
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (view);

	if (priv->completion)
	{
		g_object_run_dispose (G_OBJECT (priv->completion));
		g_clear_object (&priv->completion);
	}

	g_clear_pointer (&priv->font_desc, pango_font_description_free);

	_gtk_source_view_assistants_shutdown (&priv->assistants);

	G_OBJECT_CLASS (gtk_source_view_parent_class)->dispose (object);
}

static void
gtk_source_view_constructed (GObject *object)
{
	GtkSourceView *self = (GtkSourceView *)object;

	G_OBJECT_CLASS (gtk_source_view_parent_class)->constructed (object);

	gtk_widget_action_set_enabled (GTK_WIDGET (self), "completion.show", TRUE);
}

static void
gtk_source_view_class_init (GtkSourceViewClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->constructed = gtk_source_view_constructed;
	object_class->dispose = gtk_source_view_dispose;

	widget_class->size_allocate = gtk_source_view_size_allocate;

	gtk_widget_class_install_action (widget_class, "source.show-completion", NULL, show_completion_cb);

	gtk_widget_class_add_binding_action (widget_class,
	                                     GDK_KEY_space,
	                                     GDK_CONTROL_MASK,
	                                     "source.show-completion",
	                                     NULL);
}

static void
gtk_source_view_init (GtkSourceView *view)
{
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (view);
	GtkEventController *key;
	g_autofree gchar *css = NULL;
	g_autofree gchar *css_inner = NULL;

	priv->font_desc = pango_font_description_from_string ("Monospace 10");
	css_inner = _gtk_source_utils_pango_font_description_to_css (priv->font_desc);
	css = g_strdup_printf ("textview { %s }", css_inner);
	
	priv->css_provider = gtk_css_provider_new ();
	gtk_css_provider_load_from_data (priv->css_provider, css, -1);
	gtk_style_context_add_provider (gtk_widget_get_style_context (GTK_WIDGET (view)),
	                                GTK_STYLE_PROVIDER (priv->css_provider),
	                                GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	_gtk_source_view_assistants_init (&priv->assistants, view);

	key = gtk_event_controller_key_new ();
	gtk_event_controller_set_propagation_phase (key, GTK_PHASE_CAPTURE);
	g_signal_connect (key, "key-pressed", G_CALLBACK (on_key_pressed_cb), view);
	gtk_widget_add_controller (GTK_WIDGET (view), key);
}

void
_gtk_source_view_add_assistant (GtkSourceView      *view,
			       	GtkSourceAssistant *assistant)
{
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (view);

	g_return_if_fail (GTK_SOURCE_IS_VIEW (view));
	g_return_if_fail (GTK_SOURCE_IS_ASSISTANT (assistant));

	_gtk_source_view_assistants_add (&priv->assistants, assistant);
}

void
_gtk_source_view_remove_assistant (GtkSourceView      *view,
				   GtkSourceAssistant *assistant)
{
	GtkSourceViewPrivate *priv = gtk_source_view_get_instance_private (view);

	g_return_if_fail (GTK_SOURCE_IS_VIEW (view));
	g_return_if_fail (GTK_SOURCE_IS_ASSISTANT (assistant));

	_gtk_source_view_assistants_remove (&priv->assistants, assistant);
}

GtkSourceCompletion *
gtk_source_view_get_completion (GtkSourceView *self)
{
	g_return_val_if_fail (GTK_SOURCE_IS_VIEW (self), NULL);

	return get_completion (self);
}
