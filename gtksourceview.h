#pragma once

#include "gtksourcetypes.h"

G_BEGIN_DECLS

#define GTK_SOURCE_TYPE_VIEW (gtk_source_view_get_type())

G_DECLARE_DERIVABLE_TYPE (GtkSourceView, gtk_source_view, GTK_SOURCE, VIEW, GtkTextView)

struct _GtkSourceViewClass
{
	GtkTextViewClass parent_class;

	gpointer _reserved[20];
};

GtkWidget *gtk_source_view_new (void);
GtkSourceCompletion *gtk_source_view_get_completion (GtkSourceView *view);

G_END_DECLS
