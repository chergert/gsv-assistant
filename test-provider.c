#include "test-provider.h"

#include "gtksourcecompletion.h"
#include "gtksourcecompletioncell.h"
#include "gtksourcecompletioncontext.h"
#include "gtksourcecompletionproposal.h"

struct _TestProvider
{
  GObject parent_instance;
};

struct _TestProposal
{
	GObject parent_instance;
	const char *return_type;
	const char *display;
	const char *word;
	int type;
	guint deprecated : 1;
};

enum {
	KEYWORD,
	TYPE,
	FUNCTION,
	PREPROCESSOR,
};

static const struct {
	int type;
	const char *typed_text;
	const char *return_type;
	const char *display;
	gboolean deprecated;
} items[] = {
	{ FUNCTION, "gtk_widget_new", "GtkWidget *", "gtk_widget_new (void)" },
	{ FUNCTION, "gtk_box_new", "GtkWidget *", "gtk_box_new (void)" },
	{ FUNCTION, "gtk_text_view_new", "GtkWidget *", "gtk_text_view_new (void)" },
	{ FUNCTION, "gtk_label_new", "GtkWidget *", "gtk_label_new (const char *label)" },
	{ FUNCTION, "g_autoptr", NULL, "g_autoptr(Type)" },
	{ FUNCTION, "g_auto", NULL, "g_auto(Type)" },
	{ FUNCTION, "g_return_if_fail", NULL, "g_return_if_fail (expr)" },
	{ FUNCTION, "g_return_val_if_fail", NULL, "g_return_val_if_fail (expr)" },
	{ FUNCTION, "G_IS_OBJECT", "gboolean", "G_IS_OBJECT (obj)" },
	{ FUNCTION, "GTK_IS_WIDGET", "gboolean", "GTK_IS_WIDGET (obj)" },
	{ FUNCTION, "g_thread_init", "void", "g_thread_init (void)", TRUE },
	{ FUNCTION, "gtk_widget_show", "void", "gtk_widget_show (GtkWidget *widget)", FALSE },
	{ FUNCTION, "gtk_widget_hide", "void", "gtk_widget_hide (GtkWidget *widget)", FALSE },
	{ FUNCTION, "g_thread_new", "GThread *", "g_thread_new (const char *name)", FALSE },
	{ FUNCTION, "DoSomething", "void", "DoSomething (void)" },

	{ KEYWORD, "break" },
	{ KEYWORD, "case" },
	{ KEYWORD, "const" },
	{ KEYWORD, "do" },
	{ KEYWORD, "else" },
	{ KEYWORD, "enum" },
	{ KEYWORD, "goto" },
	{ KEYWORD, "if" },
	{ KEYWORD, "return" },
	{ KEYWORD, "static" },
	{ KEYWORD, "struct" },
	{ KEYWORD, "switch" },
	{ KEYWORD, "typedef" },
	{ KEYWORD, "union" },
	{ KEYWORD, "while" },

	{ TYPE, "void" },
	{ TYPE, "char" },
	{ TYPE, "int" },
	{ TYPE, "unsigned" },
	{ TYPE, "long" },
	{ TYPE, "float" },
	{ TYPE, "double" },
	{ TYPE, "short" },
	{ TYPE, "gint8" },
	{ TYPE, "gint16" },
	{ TYPE, "gint32" },
	{ TYPE, "gint64" },
	{ TYPE, "guint8" },
	{ TYPE, "guint16" },
	{ TYPE, "guint32" },
	{ TYPE, "guint64" },
	{ TYPE, "GError", "struct" },
	{ TYPE, "gpointer", "void *" },
	{ TYPE, "GAsyncResult" },
	{ TYPE, "GAsyncReadyCallback", "void", "(*GAsyncReadyCallback) (GObject *, GAsyncResult *, gpointer)" },
	{ TYPE, "GCancellable", "struct" },
	{ TYPE, "GtkWidget", "struct" },

	{ PREPROCESSOR, "FALSE" },
	{ PREPROCESSOR, "TRUE" },
	{ PREPROCESSOR, "NULL" },
};

static inline gboolean
matches (const char *haystack,
         const char *needle)
{
	return g_str_has_prefix (haystack, needle) && !g_str_equal (haystack, needle);
}

static GListModel *
test_provider_populate (GtkSourceCompletionProvider  *provider,
                        GtkSourceCompletionContext   *context,
                        GError                      **error)
{
	GListStore *store = g_list_store_new (TEST_TYPE_PROPOSAL);
	GtkTextIter begin, end;

	if (gtk_source_completion_context_get_bounds (context, &begin, &end))
	{
		char *word = gtk_text_iter_get_slice (&begin, &end);

		for (guint i = 0; i < G_N_ELEMENTS (items); i++)
		{
			if (!word[0] || matches (items[i].typed_text, word))
			{
				TestProposal *proposal = g_object_new (TEST_TYPE_PROPOSAL, NULL);
				proposal->word = items[i].typed_text;
				proposal->display = items[i].display ?: items[i].typed_text;
				proposal->deprecated = items[i].deprecated;
				proposal->return_type = items[i].return_type;
				proposal->type = items[i].type;
				g_list_store_append (store, proposal);
				g_object_unref (proposal);
			}
		}

		g_free (word);
	}

	return G_LIST_MODEL (store);
}

static void
test_provider_display (GtkSourceCompletionProvider *provider,
                       GtkSourceCompletionContext  *context,
                       GtkSourceCompletionProposal *proposal,
                       GtkSourceCompletionCell     *cell)
{
	switch (gtk_source_completion_cell_get_column (cell))
	{
	case GTK_SOURCE_COMPLETION_COLUMN_ICON:
		switch (TEST_PROPOSAL (proposal)->type)
		{
		case KEYWORD:
			gtk_source_completion_cell_set_icon_name (cell, NULL);
			break;

		case FUNCTION:
			gtk_source_completion_cell_set_icon_name (cell, "lang-function-symbolic");
			break;

		case TYPE:
			gtk_source_completion_cell_set_icon_name (cell, "lang-typedef-symbolic");
			break;

		case PREPROCESSOR:
			gtk_source_completion_cell_set_icon_name (cell, "lang-define-symbolic");
			break;

		default:
			break;
		}
		break;

	case GTK_SOURCE_COMPLETION_COLUMN_BEFORE:
		gtk_source_completion_cell_set_text (cell, TEST_PROPOSAL (proposal)->return_type);
		break;

	case GTK_SOURCE_COMPLETION_COLUMN_AFTER:
		if (TEST_PROPOSAL (proposal)->deprecated)
			gtk_source_completion_cell_set_icon_name (cell, "dialog-warning-symbolic");
		else
			gtk_source_completion_cell_set_icon_name (cell, NULL);
		break;

	case GTK_SOURCE_COMPLETION_COLUMN_TYPED_TEXT: {
		g_autofree gchar *word = gtk_source_completion_context_get_word (context);
		g_autofree gchar *casefold = g_utf8_casefold (word, -1);
		PangoAttrList *attrs = gtk_source_completion_fuzzy_highlight (TEST_PROPOSAL (proposal)->display, casefold);

		if (TEST_PROPOSAL (proposal)->deprecated)
		{
			if (attrs == NULL)
			{
				attrs = pango_attr_list_new ();
			}

			pango_attr_list_insert (attrs, pango_attr_strikethrough_new (TRUE));
		}

		gtk_source_completion_cell_set_text_with_attributes (cell, TEST_PROPOSAL (proposal)->display, attrs);
		g_clear_pointer (&attrs, pango_attr_list_unref);

		break;
	}

	case GTK_SOURCE_COMPLETION_COLUMN_COMMENT:
		switch (TEST_PROPOSAL (proposal)->type)
		{
		case FUNCTION:
			gtk_source_completion_cell_set_text (cell, "This is a long comment about the function which might provide some quick details not necessarily requiring viewing the whole details.");
			break;

		default:
			gtk_source_completion_cell_set_widget (cell, NULL);
			break;
		}
		break;

	case GTK_SOURCE_COMPLETION_COLUMN_DETAILS:
		if (TEST_PROPOSAL (proposal)->type == FUNCTION)
		{
			g_autofree gchar *comment = g_strdup_printf ("<b>%s</b>\n\n%s",
								     TEST_PROPOSAL (proposal)->word,
								     TEST_PROPOSAL (proposal)->display);
			gtk_source_completion_cell_set_markup (cell, comment);
		}
		else
		{
			gtk_source_completion_cell_set_widget (cell, NULL);
		}
		break;

	default:
		break;
	}
}

static gboolean
test_provider_refilter (GtkSourceCompletionProvider *provider,
                        GtkSourceCompletionContext  *context,
                        GListModel                  *results)
{
	g_autofree char *word = NULL;
	g_autofree char *casefold = NULL;
	guint n_items;

	g_assert (GTK_SOURCE_IS_COMPLETION_PROVIDER (provider));
	g_assert (GTK_SOURCE_IS_COMPLETION_CONTEXT (context));
	g_assert (G_IS_LIST_MODEL (results));

	word = gtk_source_completion_context_get_word (context);
	casefold = g_utf8_casefold (word, -1);
	n_items = g_list_model_get_n_items (results);

	for (guint i = n_items; i > 0; i--)
	{
		g_autoptr(TestProposal) proposal = g_list_model_get_item (results, i - 1);
		guint prio = 0;

		if (!gtk_source_completion_fuzzy_match (proposal->word, casefold, &prio))
		{
			g_list_store_remove (G_LIST_STORE (results), i - 1);
		}
	}

	/* Mark all items as changed to get highlight updates */
	n_items = g_list_model_get_n_items (results);
	if (n_items > 0)
	{
		g_list_model_items_changed (results, 0, n_items, n_items);
	}

	return TRUE;
}

static void
test_provider_activate (GtkSourceCompletionProvider *provider,
                        GtkSourceCompletionContext  *context,
			GtkSourceCompletionProposal *proposal)
{
	const char *word = TEST_PROPOSAL (proposal)->word;
	GtkTextBuffer *buffer;
	GtkTextIter begin, end;

	gtk_source_completion_context_get_bounds (context, &begin, &end);
	buffer = gtk_text_iter_get_buffer (&begin);

	gtk_text_buffer_begin_user_action (buffer);
	gtk_text_buffer_delete (buffer, &begin, &end);
	gtk_text_buffer_insert (buffer, &begin, word, -1);
	gtk_text_buffer_end_user_action (buffer);
}

static gboolean
test_provider_key_activates (GtkSourceCompletionProvider *self,
			     GtkSourceCompletionContext  *context,
			     GtkSourceCompletionProposal *proposal,
                             guint                        keyval,
                             GdkModifierType              state)
{
	return keyval == '<' || keyval == '(' || keyval == '.' || keyval == '-';
}

static GPtrArray *
test_provider_list_alternates (GtkSourceCompletionProvider *self,
                               GtkSourceCompletionContext  *context,
                               GtkSourceCompletionProposal *proposal)
{
	TestProposal *p = TEST_PROPOSAL (proposal);
	GPtrArray *ar;
	static const struct {
		const char *typed_text;
		const char *return_value;
		const char *display;
	} alternates[] = {
		{ "DoSomething", "int", "DoSomething (int v)" },
		{ "DoSomething", "uint", "DoSomething (uint v)" },
		{ "DoSomething", "int64", "DoSomething (int64 v)" },
		{ "DoSomething", "uint64", "DoSomething (uint64 v)" },
	};

	if (!g_str_equal (p->word, "DoSomething"))
	{
		return NULL;
	}

	ar = g_ptr_array_new ();

	for (guint i = 0; i < G_N_ELEMENTS (alternates); i++)
	{
		p = g_object_new (TEST_TYPE_PROPOSAL, NULL);
		p->word = "DoSomething";
		p->display = alternates[i].display;
		p->return_type = alternates[i].return_value;
		p->type = FUNCTION;

		g_ptr_array_add (ar, p);
	}

	return ar;
}

static void
provider_iface_init (GtkSourceCompletionProviderInterface *iface)
{
	iface->populate = test_provider_populate;
	iface->display = test_provider_display;
	iface->refilter = test_provider_refilter;
	iface->activate = test_provider_activate;
	iface->key_activates = test_provider_key_activates;
	iface->list_alternates = test_provider_list_alternates;
}

G_DEFINE_TYPE_WITH_CODE (TestProvider, test_provider, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GTK_SOURCE_TYPE_COMPLETION_PROVIDER, provider_iface_init))
G_DEFINE_TYPE_WITH_CODE (TestProposal, test_proposal, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GTK_SOURCE_TYPE_COMPLETION_PROPOSAL, NULL))

static void
test_provider_class_init (TestProviderClass *klass)
{
}

static void
test_provider_init (TestProvider *self)
{
}

static void
test_proposal_class_init (TestProposalClass *klass)
{
}

static void
test_proposal_init (TestProposal *self)
{
}

GtkSourceCompletionProvider *
test_provider_new (void)
{
	return g_object_new (TEST_TYPE_PROVIDER, NULL);
}
