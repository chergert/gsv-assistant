#include "gtksourcecompletion.h"
#include "gtksourceview.h"

#include "test-provider.h"

static GMainLoop *main_loop;

static void
source_init (void)
{
  GtkCssProvider *provider;
  const char *data;
  GBytes *bytes;

  bytes = g_resources_lookup_data ("/org/gnome/gtksourceview/GtkSourceView.css", 0, NULL);
  data = g_bytes_get_data (bytes, NULL);

  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_data (provider, data, strlen (data));
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  gtk_icon_theme_add_resource_path (gtk_icon_theme_get_for_display (gdk_display_get_default ()),
                                    "/org/gnome/gtksourceview/icons/");
}

gint
main (gint argc,
      gchar *argv[])
{
	GtkWindow *window;
	GtkScrolledWindow *scroller;
	GtkSourceView *view;
	GtkHeaderBar *bar;
        GtkSourceCompletionProvider *provider;

	main_loop = g_main_loop_new (NULL, FALSE);

	gtk_init ();
  source_init ();

	window = g_object_new (GTK_TYPE_WINDOW,
	                       "default-width", 800,
	                       "default-height", 600,
                         "title", "Assistants Test",
	                       NULL);
	bar = g_object_new (GTK_TYPE_HEADER_BAR,
	                    "show-title-buttons", TRUE,
	                    NULL);
	scroller = g_object_new (GTK_TYPE_SCROLLED_WINDOW, NULL);
	view = GTK_SOURCE_VIEW (gtk_source_view_new ());

        provider = test_provider_new ();
        gtk_source_completion_add_provider (gtk_source_view_get_completion (view), provider);

	gtk_window_set_titlebar (window, GTK_WIDGET (bar));
	gtk_window_set_child (window, GTK_WIDGET (scroller));
	gtk_scrolled_window_set_child (scroller, GTK_WIDGET (view));
	g_signal_connect_swapped (window, "close-request", G_CALLBACK (g_main_loop_quit), main_loop);
	gtk_window_present (window);

	g_main_loop_run (main_loop);

	return 0;
}
