#pragma once

#include "gtksourcecompletionprovider.h"

G_BEGIN_DECLS

#define TEST_TYPE_PROVIDER (test_provider_get_type())
#define TEST_TYPE_PROPOSAL (test_proposal_get_type())

G_DECLARE_FINAL_TYPE (TestProvider, test_provider, TEST, PROVIDER, GObject)
G_DECLARE_FINAL_TYPE (TestProposal, test_proposal, TEST, PROPOSAL, GObject)

GtkSourceCompletionProvider *test_provider_new (void);

G_END_DECLS
